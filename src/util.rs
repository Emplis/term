// https://en.wikipedia.org/wiki/ANSI_escape_code

use core::fmt;
use std::io::{self, Write};

const ESC: &str = "\x1b";

trait Code {
    fn get_code(&self) -> String;
}

macro_rules! impl_fmt_display {
    () => {};
    ($t:ident $(,)?) => {
        impl fmt::Display for $t {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(f, "{}", self.get_code())
            }
        }
    };
}

macro_rules! define_color {
    () => {};
    ($type_name:ident, $custom_color_code:tt $(, $colors:tt, $codes:tt)+ $(,)?) => {
        #[derive(Clone, Copy, Debug)]
        pub enum $type_name {
            $($colors,)*
            /// 256-color mode
            EightBit(u8),
            /// Red, Green, Blue
            RGB(u8, u8, u8),
            /// Cyan, Magenta, Yellow
            CMY(u8, u8, u8),
        }

        impl Code for $type_name {
            fn get_code(&self) -> String {
                match self {
                    $(Self::$colors => format!("{}[{}m", ESC, $codes),)*
                    Self::EightBit(n) => format!("{}[{};5;{}m", ESC, $custom_color_code, n),
                    Self::RGB(r, g, b) => format!("{}[{};2;{};{};{}m", ESC, $custom_color_code, r, g, b),
                    Self::CMY(c, m, y) => format!("{}[{};3;{};{};{}m", ESC, $custom_color_code, c, m, y),
                }
            }
        }

        impl_fmt_display!($type_name);
    };
}

#[cfg_attr(rustfmt, rustfmt_skip)]
define_color!(
    FgColor, 38,
    Black, 30, Red, 31, Green, 32, Yellow, 33, Blue, 34, Magenta, 35, Cyan, 36, White, 37,
    BrightBlack, 30, BrightRed, 31, BrightGreen, 32, BrightYellow, 33, BrightBlue, 34,
    BrightMagenta, 35, BrightCyan, 36, BrightWhite, 37
);

#[cfg_attr(rustfmt, rustfmt_skip)]
define_color!(
    BgColor, 48,
    Black, 40, Red, 41, Green, 42, Yellow, 43, Blue, 44, Magenta, 45, Cyan, 46, White, 47,
    BrightBlack, 100, BrightRed, 101, BrightGreen, 102, BrightYellow, 103, BrightBlue, 104,
    BrightMagenta, 105, BrightCyan, 106, BrightWhite, 107
);

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Text {
    Reset = 0,
    Bold,
    Dim,
    Italic,
    Underline,
    SlowBlink,
    FastBlink,
    Reverse,
    Hide,
    Strike,
    Overlined = 53,
}

impl Code for Text {
    fn get_code(&self) -> String {
        format!("{}[{}m", ESC, *self as u8)
    }
}

impl_fmt_display!(Text);

#[derive(Clone, Copy)]
pub enum Cursor {
    /// Moves the cursor *n* cells in the given direction.
    /// If the cursor is already at the edge of the screen, this has no effect.
    Up(u8),
    /// Moves the cursor *n* cells in the given direction.
    /// If the cursor is already at the edge of the screen, this has no effect.
    Down(u8),
    /// Moves the cursor *n* cells in the given direction.
    /// If the cursor is already at the edge of the screen, this has no effect.
    Forward(u8),
    /// Moves the cursor *n* cells in the given direction.
    /// If the cursor is already at the edge of the screen, this has no effect.
    Backward(u8),
    /// Moves cursor to beginning of the line *n* lines down. (not ANSI.SYS)
    NextLine(u8),
    /// Moves cursor to beginning of the line *n* lines up. (not ANSI.SYS)
    PrevLine(u8),
    /// Moves the cursor to column *n*. (not ANSI.SYS)
    AbsCol(u8),
    /// Moves the cursor to row *n*, column *m*.
    /// The values are 1-based (top left corner).
    Pos(u8, u8),
}

impl Code for Cursor {
    fn get_code(&self) -> String {
        match self {
            Self::Up(n) => format!("{}[{}A", ESC, n),
            Self::Down(n) => format!("{}[{}B", ESC, n),
            Self::Forward(n) => format!("{}[{}C", ESC, n),
            Self::Backward(n) => format!("{}[{}D", ESC, n),
            Self::NextLine(n) => format!("{}[{}E", ESC, n),
            Self::PrevLine(n) => format!("{}[{}F", ESC, n),
            Self::AbsCol(n) => format!("{}[{}G", ESC, n),
            Self::Pos(n, m) => format!("{}[{};{}H", ESC, n, m),
        }
    }
}

impl_fmt_display!(Cursor);

pub enum Display {
    /// Clears part of the screen.
    /// - If *n* is `0`, clear from cursor to end of screen.
    /// - If *n* is `1`, clear from cursor to beginning of the screen.
    /// - If *n* is `2`, clear entire screen (and moves cursor to upper left on DOS ANSI.SYS).
    /// - If *n* is `3`, clear entire screen and delete all lines saved in the scrollback buffer
    /// (this feature was added for xterm and is supported by other terminal applications).
    Erase(u8),
    /// Erases part of the line.
    /// - If *n* is `0`, clear from cursor to the end of the line.
    /// - If *n* is `1`, clear from cursor to beginning of the line.
    /// - If *n* is `2`, clear entire line.
    ///
    /// Cursor position does not change.
    EraseLine(u8),
    /// Scroll whole page up by *n* lines. New lines are added at the bottom. (not ANSI.SYS)
    ScrollUp(u8),
    /// Scroll whole page down by *n* lines. New lines are added at the top. (not ANSI.SYS)
    ScrollDown(u8),
}

impl Code for Display {
    fn get_code(&self) -> String {
        match self {
            Self::Erase(n) => format!("{}[{}J", ESC, n),
            Self::EraseLine(n) => format!("{}[{}K", ESC, n),
            Self::ScrollUp(n) => format!("{}[{}S", ESC, n),
            Self::ScrollDown(n) => format!("{}[{}T", ESC, n),
        }
    }
}

impl_fmt_display!(Display);

#[derive(Clone, Copy)]
pub enum AlternateScreenBuffer {
    /// Save cursor and then switch to the alternate screen buffer, clearing it first.
    Enable,
    /// Switch to normal screen buffer and restore cursor.
    Disable,
}

impl Code for AlternateScreenBuffer {
    fn get_code(&self) -> String {
        match self {
            Self::Enable => format!("{}[?1049h", ESC),
            Self::Disable => format!("{}[?1049l", ESC),
        }
    }
}

impl_fmt_display!(AlternateScreenBuffer);

pub fn switch_to_alternate_screen_buf() {
    let buf: Vec<u8> = AlternateScreenBuffer::Enable.get_code().into_bytes();
    io::stdout()
        .write(&buf)
        .expect("Error: `write` fail. (I/O error)");
    io::stdout()
        .flush()
        .expect("Error: `flush` fail. (I/O error)");
}

pub fn switch_to_normal_screen_buf() {
    let buf: Vec<u8> = AlternateScreenBuffer::Disable.get_code().into_bytes();
    io::stdout()
        .write(&buf)
        .expect("Error: `write` fail. (I/O error)");
    io::stdout()
        .flush()
        .expect("Error: `flush` fail. (I/O error)");
}
