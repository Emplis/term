use libc;

/// return (rows, cols)
pub fn get_win_size() -> (u16, u16) {
    let w = libc::winsize {
        ws_row: 0,
        ws_col: 0,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };

    unsafe {
        libc::ioctl(libc::STDOUT_FILENO, libc::TIOCGWINSZ, &w);
    }

    (w.ws_row, w.ws_col)
}
